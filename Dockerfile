FROM python:3.9-slim-buster

# dependency for installing jdk
RUN mkdir -p /usr/share/man/man1

# Install OpenJDK-11
RUN apt-get update && \
    apt-get install -y openjdk-11-jdk && \
    apt-get install -y ant && \
    apt-get install -y git && \
    apt-get clean;

# Fix certificate issues
RUN apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f;

# Setup JAVA_HOME -- useful for docker commandline
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/
RUN export JAVA_HOME

# Setup JENA_HOME and PATH
ENV JENA_HOME /code/app/data/apache-jena-4.5.0
RUN export PATH=$PATH:$JENA_HOME/bin


WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./app /code/app/
RUN git -C /code/app/data/ clone https://github.com/cwrc/RDF-extraction.git 
# Uncomment next line to run locally
#COPY ./.env /code/.env
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
