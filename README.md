# LEAF-LINCS Bridge

LINCS service to process data from LEAF and send it to the LINCS triplestore.

To run the application locally, you will need to get a `.env` file from LINCS or set the following environment variables appropriately for your setup. You will also need to have a local ResearchSpace instance running. 

```
S3_ACCESS
S3_SECRET
RS_USER
RS_SECRET
S3_PATH
RS_SPARQL
DEPLOY_TARGET="review"
```

## Running locally with Docker

To run locally, uncomment the line in the Dockerfile that copies the .env file.

```docker build --no-cache -t leafimage .```

```docker run -d --name leafcontainer -p 80:80 leafimage```

The service should be accessible at `http://0.0.0.0:80`

## Example request

There are Postman Collections containing examples requests in `sample_data/`. You will need to setup basic auth credentials for your local instance or request credentials from LINCS.