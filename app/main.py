from fastapi import FastAPI
import app.routes as routes
import logging

app = FastAPI()
app.include_router(routes.router)

@app.get("/healthz", status_code=200)
def check_api_health():
    # for key in logging.Logger.manager.loggerDict:
    #     # only log messages that are at least a warning
    #     logging.getLogger(key).setLevel(logging.DEBUG)
    return {"message": "API Health Check"}
