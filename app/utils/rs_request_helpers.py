import os
import requests
from asyncio.log import logger
from dotenv import load_dotenv
from requests.adapters import HTTPAdapter, Retry
from app.utils.request_helpers import leaf_pipeline_logging


load_dotenv()
logger = leaf_pipeline_logging('rs_request_helpers.py')


def get_post_url():
	return os.getenv('RS_SPARQL')


def get_rs_info():
	post_url = get_post_url()
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
	}
	auth = (os.getenv('RS_USER'), os.getenv('RS_SECRET'))
	return post_url, headers, auth


# Function returns the required information for a LOAD request to RS
# Use a SPARLQL Query to create a new graph and load data into it
def upload_graph_request_data(s3_file_name: str, graph_name: str):
	post_url, headers, auth = get_rs_info()
	get_url_path = os.getenv('S3_PATH')
	s3_url_path = f'<{get_url_path}' + s3_file_name + '>'
	data = 'update=LOAD ' + s3_url_path + ' INTO GRAPH  <http://graph.lincsproject.ca/' + graph_name + '>'
	return post_url, headers, data, auth


# Function Returns the required information for the request for RS.
# Utlize a SPARQL Query to Clear a Graph
def clear_graph_request_data(graph_name: str):
	post_url, headers, auth = get_rs_info()
	data = 'update=CLEAR GRAPH<http://graph.lincsproject.ca/' + graph_name + '>'
	return post_url, headers, data, auth


# This checks if a graph exists
def check_graph_exists(graph_name: str):
	post_url, headers, auth = get_rs_info()
	data = 'query=ASK WHERE { GRAPH <http://graph.lincsproject.ca/' + graph_name + '> { ?s ?p ?o } }'
	response_code, response_text = try_rs_request(post_url, headers, data, auth, "")
	if response_code != 200:
		logger.info(f"\nUnable to retrieve graph '{graph_name} with query {data} to RS endpoint {post_url}'")
		logger.info(f"\n{response_code} {response_text}")
		return 502
	check_true = response_text.find("<boolean>true</boolean>")
	check_false = response_text.find("<boolean>false</boolean>")
	if check_true > -1:
		logger.info(f"Connected to RS: Graph {graph_name} exists")
		return True
	elif check_false > -1:
		logger.info(f"Connected to RS: Graph {graph_name} doesn't exist")
		return False


# Copies graph in RS with name graph_name to a new graph called graph_name_temp
# Deletes original graph graph_name
# TODO confirm that if this fails then the original graph will be maintained
def move_graph(old_graph_name: str, new_graph_name: str):
	post_url, headers, auth = get_rs_info()
	data = f'update=MOVE <http://graph.lincsproject.ca/{old_graph_name}> TO <http://graph.lincsproject.ca/{new_graph_name}_temp>'
	return post_url, headers, data, auth


def drop_rs_graph(graph_name: str):
	post_url, headers, auth = get_rs_info()
	data = 'update=DROP GRAPH<http://graph.lincsproject.ca/' + graph_name + '>'
	return post_url, headers, data, auth


# Send requests to ResearchSpace
def try_rs_request(
	post_url: str,
	headers: str,
	data: str,
	auth: tuple[str, str],
	file_job_id: str):

	# Request will run in a total of 2 times 1 as the original request and where total = 1 where it tries again once more
	# Request tries 2 times
	# It will try 1 more time after 60 seconds again
	retries = Retry(
		total=1,
		backoff_factor=60,
		status_forcelist=[400, 404, 500, 502, 503, 504, 429])
	request = requests.Session()
	try:
		request.mount('https://', HTTPAdapter(max_retries=retries))
		response = request.post(
			post_url,
			headers=headers,
			data=data,
			auth=auth)
		return response.status_code, response.text
	except Exception as err:
		logger.error(f"[Job {file_job_id}] Request to ResearchSpace unsuccessful:  {err}")
		return 502, (f'[Job {file_job_id}] Request to ResearchSpace unsuccessful:  {err}')
