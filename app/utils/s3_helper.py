import os
from asyncio.log import logger
from dotenv import load_dotenv
from minio import Minio
from app.utils.request_helpers import leaf_pipeline_logging


load_dotenv()
logger = leaf_pipeline_logging('s3_helper.py')


# Get Login info from env file to log into S3
def get_s3_credentials():
	access = os.environ.get("S3_ACCESS")
	secret = os.environ.get("S3_SECRET")
	return access, secret


# Connect to Minio S3 Storage where files are stored
def minio_init(access, secret):
	return Minio(
		'aux.lincsproject.ca',
		access_key=access,
		secret_key=secret,
		secure=True,
		region='ca')


def get_minio_client():
	access, secret = get_s3_credentials()
	client = minio_init(access, secret)
	return client


def get_env_bucket():
	# Get the name used within S3 paths based on the deployment (review, stage, prod)

	deploy_target = os.environ.get("DEPLOY_TARGET")

	if "review" in deploy_target:
		env_bucket = "dev"
	elif "stag" in deploy_target:
		env_bucket = "stage"
	else:
		env_bucket = "prod"

	return env_bucket


# Get the bucket for S3 the file path for S3 and the local file path here
# Function gets the paths for in where the converted turtle files + the logs for the orlando entity + the s3 bucket
def get_s3_paths(project_string: str = None, orlando_check: bool = False, public: str = "private"):
	bucket = "public"
	env_path = get_env_bucket()
	if project_string is not None:
		s3_file_path = f"leaf-api/{env_path}/converted/{project_string}/{public}/"
	else:
		s3_file_path = f"leaf-api/{env_path}/converted/"
	s3_file_path_orlando_logs = ""
	if orlando_check:
		s3_file_path_orlando_logs = f"leaf-api/{env_path}/logs/orlando_entries/"
	return bucket, s3_file_path, s3_file_path_orlando_logs


# Upload a object or file to 3 and checking if there was a split or not
def upload_to_s3(
	client: Minio,
	bucket_name: str,
	local_file_path: str,
	s3_dst_path: str,
	file_job_id: str,
	file_name: str,
	num_files: int):

	# Retry if fail
	err = ""
	for i in range(1, 3):
		try:
			# If num_file is greater than 0 means the file has been split
			# Loop through each split file and upload to S3
			if num_files > 0:
				for i in range(num_files):
					current_file_str = f"{file_job_id}_split_file_{i}.ttl"
					client.fput_object(bucket_name, f"{s3_dst_path}{current_file_str}", f"{local_file_path}{current_file_str}")
					logger.info(f'[Job {file_job_id}] [{file_job_id}_split_file_{i}.ttl] Data succesfully uploaded to S3.')
			else:
				# Otherwise just upload the file that was generated
				current_file_str = file_name
				client.fput_object(bucket_name, f"{s3_dst_path}{current_file_str}", f"{local_file_path}{file_name}")
			logger.info(f'[Job {file_job_id}] Data successfully uploaded to S3.')
			return True, err
		except Exception as err:
			logger.info(f'[Job {file_job_id}] [{current_file_str}] Error, unable to upload to S3:  {err}')
			return False, err
	else:
		return False, err


# Remove an object or file from S3
def remove_from_s3(
	client: Minio,
	bucket_name: str,
	s3_dst_path: str,
	file_name: str,
	file_job_id: str,
	num_files: int):

	# Try again if fail
	for i in range(1, 3):
		try:
			current_file_str = ""
			# If number of files is greater than 0 means the file has been split
			# Loop through each split file and remove from S3
			if num_files > 0:
				for i in range(num_files):
					current_file_str = f"{file_job_id}_split_file_{i}.ttl"
					client.remove_object(bucket_name, f"{s3_dst_path}{current_file_str}")
					logger.info(f'[Job {file_job_id}] [{current_file_str}] Successfully removed from S3.')
			# Otherwise remove the uploaded file from S3
			else:
				current_file_str = file_name
				client.remove_object(bucket_name, f"{s3_dst_path}{current_file_str}")
				logger.info(f'[Job {file_job_id}] [{current_file_str}] Successfully removed from S3.')
			return True
		except Exception as err:
			logger.info(f'[[Job {file_job_id}] {current_file_str}] Error deleting from S3:  {err}')
			return False
	else:
		return False


# This function is used to remove a object from S3
# Difference is that the object includes the file path
# (Due to the list objects from S3 since it concatenates the object + file path)
def remove_from_s3_cleanup(client: Minio, bucket_name: str, object: str, file_job_id: str):
	try:
		client.remove_object(bucket_name, object)
		logger.info(f'[Job {file_job_id}] [{object}] Successfully removed from S3.')
		return 1
	except Exception as err:
		logger.info(f'[[Job {file_job_id}] {object}] Error deleting from S3:  {err}')
		return 0
