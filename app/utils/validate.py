from asyncio.log import logger
import os
import subprocess
from app.utils.request_helpers import leaf_pipeline_logging


logger = leaf_pipeline_logging('validate.py')


# Takes a file upload and runs the Jena riot script to validate the file to check if it's a valid rdf file
async def validate(file_name: list):
	# handle missing file
	if not file_name:
		return False, "Please input data file"

	# reference to the original working directory
	owd = os.getcwd()

	# change working dir to apache file
	os.chdir('app/data/apache-jena-4.5.0/bin')

	# command to create executable
	for file in file_name:
		os.system('chmod u+x ./riot')
		logger.info(f"[Job {file}] Running Script")

		# run riot command
		p = subprocess.run(['./riot', '--validate', f'{file}'], shell=False, capture_output=True)
		if p.stderr:
			os.chdir(owd)
			return False, f"{file} {p.stderr.decode('utf-8')}"
		else:
			logger.info(f"{file} is valid")
		os.chdir(owd)

	return True, "All Files Valid"
