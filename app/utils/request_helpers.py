import os
import logging
from dotenv import load_dotenv
from sys import stdout

load_dotenv()


# Removes all local Files From the bin Directory
def remove_local_file(file_path: str, file_job_id: str, num_files: int, file_name: str):
	try:
		for i in range(num_files):
			os.remove(f'{file_path}{file_job_id}_split_file_{i}.ttl')
		os.remove(f'{file_path}{file_name}')
		return f"[{file_job_id}] Input file removed from container."
	except Exception as err:
		return f"[{file_job_id}] Unable to remove input file from container:  {err}"


def get_path_to_output_ttl_path():
	# TODO this is specific to X3ML right now.
	# Could be changed to another location for other conversion workflows
	local_file_path = "app/data/apache-jena-4.5.0/bin/"
	return local_file_path


# This function is used to log values to show in the shell + Docker
def leaf_pipeline_logging(file_name: str):
	logger = logging.getLogger(file_name)
	logger.setLevel(logging.DEBUG)  # set logger level
	logFormatter = logging.Formatter\
		("%(name)-12s %(asctime)s %(levelname)-8s %(filename)s:%(funcName)s %(message)s")
	consoleHandler = logging.StreamHandler(stdout)  # set streamhandler to stdout
	consoleHandler.setFormatter(logFormatter)
	logger.addHandler(consoleHandler)
	return logger


'''
Split Function Implemented but have not been decided if the files will be split or not.
Current plan is to convert one file at a time so we are unlikely to need to split any files.

Function takes a .ttl and will split the files into smaller chunks
'''
def split_files(file_name: str, file_job_id: str):
	owd = os.getcwd()

	# change working dir to apache file
	os.chdir('app/data/apache-jena-4.5.0/bin')

	# initalize counters and write to a file
	count_triples, count_prefixes, triple_counter = 0, 0, 0

	# Read that file and count the number of dots and prefixes and append the prefixes
	prefix_list = []
	with open(file_name, "r", encoding='utf-8') as f:
		file_content = f.readlines()
	for i in range(0, len(file_content)):
		if file_content[i].startswith('@prefix'):
			prefix_list.append(file_content[i].strip("\n"))
			count_prefixes += 1
		elif file_content[i].rstrip().endswith('.'):
			count_triples += 1
	prefix_list = list(set(prefix_list))
	prefix_list.sort()

	# Count the number of triples there are and divide them up and then set the starting line to be after the prefix
	last_line = count_prefixes

	# Get the split value based on the number of files we want to have
	split_value = 300000
	result = count_triples // split_value

	# Loop to the amount of split files we want to write
	for i in range(0, result + 1):
		# write to a file write the prefix list to the file
		with open(f'{file_job_id}_split_file_' + str(i) + '.ttl', 'w', encoding='utf-8') as f_out:
			for prefix in prefix_list:
				f_out.write(prefix + "\n")
			f_out.write("\n")

			# Starting from the last line (Treating Last line variable as a pointer)
			for i in range(last_line, len(file_content)):

				# check if a string ends with a . if then add to counter until it hits a certain value
				if file_content[i].rstrip().endswith('.'):
					f_out.write(file_content[i])
					triple_counter += 1

					# Once 2000 triples are stored then set the next line to be a different value and then reset the count and break the for loop
					if triple_counter >= split_value:
						last_line = i + 1
						triple_counter = 0
						break
					# if the string doesn't end with a . simply write to the file
				else:
					f_out.write(file_content[i])
		# return on success
	os.chdir(owd)
	print(f"Total number of triples are {count_triples} {count_prefixes} {result}")
	return result + 1
