import os
import subprocess
from app.utils.request_helpers import leaf_pipeline_logging
from app.utils.s3_helper import upload_to_s3, get_minio_client
from app.utils.request_helpers import get_path_to_output_ttl_path
from asyncio.log import logger
from dotenv import load_dotenv
import shutil
import json
from datetime import datetime
import requests
# Find the specific files for a workflow conversion

load_dotenv()
logger = leaf_pipeline_logging("conversions.py")


# This function will look for a specific file in the folder app directory
def find_files(filename, search_path):
	for root, dir, files in os.walk(search_path):
		if filename in files:
			return os.path.join(root, filename)


# convert a xml input file into a valid rdf ttl file
def x3ml_conversion(
	input_file: str,
	workflow: str,
	file_name: str):

	# Get the local directory for the destination of the ttl file which will be processsed later
	dst_directory = get_path_to_output_ttl_path()

	# Get the local directories for the specific file
	jar_file_path = find_files('x3ml-engine-2.0.0-exejar.jar', "app")
	policy_file_path = find_files(f'{workflow}_policy.xml', "app")
	map_file_path = find_files(f"{workflow}_map.x3ml", "app")

	# Write to input file so the file can be processed
	with open("app/data/X3ML_conversion/input_file.xml", 'w') as f:
		f.write(input_file)
	input_files_string = find_files("input_file.xml", "app")

	# run xml conversion to ttl
	list = [
		"java",
		"-jar",
		jar_file_path,
		"-i",
		input_files_string,
		"-x",
		map_file_path,
		"-p",
		policy_file_path,
		"-o",
		f"{dst_directory}{file_name}",
		"-f",
		"text/turtle",
		"-u",
		"2"]
	subprocess.run(list, shell=False, capture_output=True)
	os.remove(input_files_string)


# This Function pulls the RDF extraction repo from and used to convert orlands xml to ttl
def git_repo():
	owd = os.getcwd()

	# change working dir to data path
	os.chdir('app/data/')

	# List the directory and check if the RDF extraction repo exists
	os.chdir("RDF-extraction/")
	list = ['git', 'pull']

	# Run the git command and output the results
	p = subprocess.run(list, capture_output=True, shell=False)
	logger.info(p.stdout.decode('utf-8'))

	# Change directory back to current directory
	os.chdir(owd)


# Orlando Conversion takes a orlando dataset xml file and converts to turtle
def orlando_conversion(input_file: str, file_name: str):
	# pull the repo from the orlando repo
	git_repo()
	owd = os.getcwd()
	dst_location = get_path_to_output_ttl_path()

	# Move to the conversion directory
	os.chdir('app/data/RDF-extraction/Biography')

	# Create the input file
	with open('input_file.xml', "w") as f:
		f.write(input_file)

	# Call the python script
	list = ['python3', 'bio_extraction.py', '-f', 'input_file.xml']
	subprocess.run(list, shell=False, capture_output=True)
	os.remove("input_file.xml")
	os.chdir(owd)

	# Find the file and rename the file to job id
	find_output_file = find_files('biography_triples.ttl', 'app')
	find_output_file_strip = find_output_file.removesuffix('biography_triples.ttl')
	os.rename(find_output_file, find_output_file_strip + file_name)
	shutil.move(find_output_file_strip + file_name, dst_location + file_name)


'''
Note: Input File variable isn't used as the only input this takes in currently is a
cofig file or else the request will be invalid so currently converting a input
xml file to a config file isn't possible
'''
# tei Conversion using post requests
def tei_conversion(input_file: str, workflow: str, file_name: str):
	# Get the destination Directory for the turtle file
	dst_directory = get_path_to_output_ttl_path()
	# Find the configuration file based on the workflow
	config_file = find_files(f'{workflow}_config.xml', "app")
	# open the configuration file and then format the output as turlle
	files = {
		'configuration': open(config_file, 'rb'),
		'format': (None, 'turtle'), }
	# Send the request and get the response and write it to the file if the status code is 200
	response = requests.post('https://app.xtriples.lincsproject.ca/exist/apps/xtriples/extract.xql', files=files)
	if response.status_code == 200:
		with open(dst_directory + f"/{file_name}", "w") as fp:
			fp.write(response.text)


# Function used to check the type of conversion that will be processed
def run_conversion(
	input_file: str,
	workflow: str,
	file_name: str):

	# open the workflow config file
	workflow_json = json.load(open("app/data/workflow_configuration/workflows.json"))
	# find if a workflow exists in workflow_json and call the specific conversion module
	if workflow in workflow_json:
		if workflow_json[workflow]['conversion'] == 'x3ml':
			x3ml_conversion(input_file=input_file, workflow=workflow, file_name=file_name)
			return False
		elif workflow_json[workflow]['conversion'] == 'orlando':
			orlando_conversion(input_file=input_file, file_name=file_name)
			return True
		elif workflow_json[workflow]['conversion'] == 'tei':
			tei_conversion(input_file=input_file, workflow=workflow, file_name=file_name)
			return False
	# Return True or false to see if the orlando conversion module is ran
	else:
		return False


# This function uploads all the orlando conversion logs into S3
def upload_orlando_logs(s3_bucket: str, file_id: str, s3_orlando_path: str):
	# get the current Date and time
	date = datetime.now().strftime("%Y_%m_%d-%I:%M:%S_%p")

	# Get the current directory and move to the directory of orlando logs
	working_direct = os.getcwd()
	s3_client = get_minio_client()
	os.chdir("app/data/RDF-extraction/Biography/log")
	list_orlando_logs = os.listdir()
	print(s3_orlando_path)
	# upload all the orlando logs files to S3
	for logs in list_orlando_logs:
		if logs != '.gitkeep':
			upload_to_s3(s3_client, s3_bucket, "", s3_orlando_path + file_id + "_" + date + "/", "", logs, 0)
	os.chdir(working_direct)
