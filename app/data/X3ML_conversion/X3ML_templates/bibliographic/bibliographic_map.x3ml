<?xml version="1.0" encoding="UTF-8"?>
<x3ml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      source_type="xpath"
      version="1.0"
      xsi:noNamespaceSchemaLocation="https://isl.ics.forth.gr/x3ml/schema/x3ml.xsd">
   <info>
      <title>3M Workshop - FINAL + BONUS (11 May 2023)_(cloned)</title>
      <general_description>The end goal mapping project (with bonus mappings) of the 3M workshop held by Jessica Ye and Natalie Hervieux on 11 May 2023 for DH@Guelph</general_description>
      <source>
         <source_info>
            <source_schema schema_file="3M_workshop_source_data.xml" type="xml" version="">3M_workshop_source_data</source_schema>
            <namespaces>
               <namespace prefix="" uri=""/>
            </namespaces>
         </source_info>
      </source>
      <target>
         <target_info>
            <target_schema schema_file="CIDOC_CRM_v7.1.1.rdfs" type="rdfs" version="">CIDOC CRM</target_schema>
            <namespaces>
               <namespace prefix="xsd" uri="http://www.w3.org/2001/XMLSchema#"/>
               <namespace prefix="rdfs" uri="http://www.w3.org/2000/01/rdf-schema#"/>
               <namespace prefix="owl" uri="https://www.w3.org/2002/07/owl#"/>
               <namespace prefix="rdf" uri="http://www.w3.org/1999/02/22-rdf-syntax-ns#"/>
               <namespace prefix="crm" uri="http://www.cidoc-crm.org/cidoc-crm/"/>
               <namespace prefix="aat" uri="http://vocab.getty.edu/aat/"/>
               <namespace prefix="cwrc" uri="http://id.lincsproject.ca/cwrc#"/>
               <namespace prefix="geonames" uri="https://sws.geonames.org/"/>
               <namespace prefix="viaf" uri="http://viaf.org/viaf/"/>
               <namespace prefix="wikidata" uri="http://www.wikidata.org/entity/"/>
            </namespaces>
         </target_info>
      </target>
      <mapping_info>
         <mapping_created_by_org/>
         <mapping_created_by_person/>
         <in_collaboration_with/>
      </mapping_info>
      <example_data_info>
         <example_data_from/>
         <example_data_contact_person/>
         <example_data_source_record/>
         <generator_policy_info/>
         <example_data_target_record/>
         <thesaurus_info/>
      </example_data_info>
   </info>
   <namespaces/>
   <mappings>
      <mapping>
         <domain>
            <source_node>/people/person</source_node>
            <target_node>
               <entity>
                  <type>crm:E21_Person</type>
                  <instance_generator name="UriExistingOrNew">
                     <arg name="uri" type="xpath">person_uri/text()</arg>
                     <arg name="text1" type="constant">person_uri</arg>
                     <arg name="uri_separator1" type="constant">/</arg>
                  </instance_generator>
                  <label_generator name="Literal">
                     <arg name="text" type="xpath">y90s_name/text()</arg>
                     <arg name="language" type="constant">en</arg>
                  </label_generator>
               </entity>
            </target_node>
         </domain>
         <link>
            <path>
               <source_relation>
                  <relation>y90s_name</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P1_is_identified_by</relationship>
                  <entity>
                     <type>crm:E33_E41_Linguistic_Appellation</type>
                     <instance_generator name="ConcatMultipleTerms">
                        <arg name="prefix" type="constant">http://temp.lincsproject.ca/</arg>
                        <arg name="sameTermsDelim" type="constant">/</arg>
                        <arg name="diffTermsDelim" type="constant">/</arg>
                        <arg name="text1" type="constant">y90s_name</arg>
                        <arg name="text2" type="xpath">text()</arg>
                     </instance_generator>
                     <label_generator name="name">
                        <arg name="name" type="xpath">text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </label_generator>
                     <additional>
                        <relationship>crm:P2_has_type</relationship>
                        <entity>
                           <type>crm:E55_Type</type>
                           <instance_generator name="UriExistingOrNew">
                              <arg name="uri" type="constant">http://id.lincsproject.ca/cwrc/personalName</arg>
                              <arg name="text1" type="constant">cwrc_personalName</arg>
                              <arg name="uri_separator1" type="constant">/</arg>
                           </instance_generator>
                           <label_generator name="Literal">
                              <arg name="text" type="constant">personal name</arg>
                              <arg name="language" type="constant">en</arg>
                           </label_generator>
                        </entity>
                     </additional>
                  </entity>
                  <relationship>crm:P190_has_symbolic_content</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>y90s_name</source_node>
               <target_node>
                  <entity>
                     <type>rdfs:Literal</type>
                     <instance_generator name="Literal">
                        <arg name="text" type="xpath">text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </instance_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>y90s_aka</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P1_is_identified_by</relationship>
                  <entity>
                     <type>crm:E33_E41_Linguistic_Appellation</type>
                     <instance_generator name="ConcatMultipleTerms">
                        <arg name="prefix" type="constant">http://temp.lincsproject.ca/</arg>
                        <arg name="sameTermsDelim" type="constant">/</arg>
                        <arg name="diffTermsDelim" type="constant">/</arg>
                        <arg name="text1" type="constant">y90s_aka</arg>
                        <arg name="text2" type="xpath">text()</arg>
                     </instance_generator>
                     <label_generator name="additional_name">
                        <arg name="name" type="xpath">../y90s_name/text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </label_generator>
                     <additional>
                        <relationship>crm:P2_has_type</relationship>
                        <entity>
                           <type>crm:E55_Type</type>
                           <instance_generator name="UriExistingOrNew">
                              <arg name="uri" type="constant">http://id.lincsproject.ca/cwrc/additionalName</arg>
                              <arg name="text1" type="constant">cwrc_additionalName</arg>
                              <arg name="uri_separator1" type="constant">/</arg>
                           </instance_generator>
                           <label_generator name="Literal">
                              <arg name="text" type="constant">additional name</arg>
                              <arg name="language" type="constant">en</arg>
                           </label_generator>
                        </entity>
                     </additional>
                  </entity>
                  <relationship>crm:P190_has_symbolic_content</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>y90s_aka</source_node>
               <target_node>
                  <entity>
                     <type>rdfs:Literal</type>
                     <instance_generator name="Literal">
                        <arg name="text" type="xpath">text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </instance_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>y90s_birth_name</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P1_is_identified_by</relationship>
                  <entity>
                     <type>crm:E33_E41_Linguistic_Appellation</type>
                     <instance_generator name="ConcatMultipleTerms">
                        <arg name="prefix" type="constant">http://temp.lincsproject.ca/</arg>
                        <arg name="sameTermsDelim" type="constant">/</arg>
                        <arg name="diffTermsDelim" type="constant">/</arg>
                        <arg name="text1" type="constant">y90s_birth_name</arg>
                        <arg name="text2" type="xpath">text()</arg>
                     </instance_generator>
                     <label_generator name="birth_name">
                        <arg name="name" type="xpath">../y90s_name/text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </label_generator>
                     <additional>
                        <relationship>crm:P2_has_type</relationship>
                        <entity>
                           <type>crm:E55_Type</type>
                           <instance_generator name="UriExistingOrNew">
                              <arg name="uri" type="constant">http://id.lincsproject.ca/cwrc/birthname</arg>
                              <arg name="text1" type="constant">cwrc_birthname</arg>
                              <arg name="uri_separator1" type="constant">/</arg>
                           </instance_generator>
                           <label_generator name="Literal">
                              <arg name="text" type="constant">birthname</arg>
                              <arg name="language" type="constant">en</arg>
                           </label_generator>
                        </entity>
                     </additional>
                  </entity>
                  <relationship>crm:P190_has_symbolic_content</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>y90s_birth_name</source_node>
               <target_node>
                  <entity>
                     <type>rdfs:Literal</type>
                     <instance_generator name="Literal">
                        <arg name="text" type="xpath">text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </instance_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>y90s_is_member_of</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P107i_is_current_or_former_member_of</relationship>
                  <entity>
                     <type>crm:E74_Group</type>
                     <instance_generator name="ConcatMultipleTerms">
                        <arg name="prefix" type="constant">http://temp.lincsproject.ca/</arg>
                        <arg name="sameTermsDelim" type="constant">/</arg>
                        <arg name="diffTermsDelim" type="constant">/</arg>
                        <arg name="text1" type="constant">group</arg>
                        <arg name="text2" type="xpath">text()</arg>
                     </instance_generator>
                     <label_generator name="Literal">
                        <arg name="text" type="xpath">text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </label_generator>
                  </entity>
                  <relationship>crm:P107_has_current_or_former_member</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>y90s_is_member_of</source_node>
               <target_node>
                  <entity>
                     <type>crm:E21_Person</type>
                     <instance_generator name="UriExistingOrNew">
                        <arg name="uri" type="xpath">../person_uri/text()</arg>
                        <arg name="text1" type="constant">person_uri</arg>
                        <arg name="uri_separator1" type="constant">/</arg>
                     </instance_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>birth</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P98i_was_born</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>birth</source_node>
               <target_node>
                  <entity variable="birth_event">
                     <type>crm:E67_Birth</type>
                     <instance_generator name="ConcatMultipleTerms">
                        <arg name="prefix" type="constant">http://temp.lincsproject.ca/</arg>
                        <arg name="sameTermsDelim" type="constant">/</arg>
                        <arg name="diffTermsDelim" type="constant">/</arg>
                        <arg name="text1" type="constant">birth_event</arg>
                        <arg name="text2" type="xpath">../y90s_name/text()</arg>
                     </instance_generator>
                     <label_generator name="birth_event">
                        <arg name="name" type="xpath">../y90s_name/text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </label_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>birth/y90s_birth_place</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P98i_was_born</relationship>
                  <entity variable="birth_event">
                     <type>crm:E67_Birth</type>
                     <instance_generator name="UUID"/>
                  </entity>
                  <relationship>crm:P7_took_place_at</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>birth/y90s_birth_place</source_node>
               <target_node>
                  <entity>
                     <type>crm:E53_Place</type>
                     <instance_generator name="UriExistingOrNew">
                        <arg name="uri" type="xpath">../y90s_birth_place_uri</arg>
                        <arg name="text1" type="constant">birth_place_uri</arg>
                        <arg name="uri_separator1" type="constant">/</arg>
                     </instance_generator>
                     <label_generator name="Literal">
                        <arg name="text" type="xpath">text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </label_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>death</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P100i_died_in</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>death</source_node>
               <target_node>
                  <entity variable="death_event">
                     <type>crm:E69_Death</type>
                     <instance_generator name="ConcatMultipleTerms">
                        <arg name="prefix" type="constant">http://temp.lincsproject.ca/</arg>
                        <arg name="sameTermsDelim" type="constant">/</arg>
                        <arg name="diffTermsDelim" type="constant">/</arg>
                        <arg name="text1" type="constant">death_event</arg>
                        <arg name="text2" type="xpath">../y90s_name/text()</arg>
                     </instance_generator>
                     <label_generator name="death_event">
                        <arg name="name" type="xpath">../y90s_name/text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </label_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>death/y90s_death_place</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P100i_died_in</relationship>
                  <entity variable="death_event">
                     <type>crm:E69_Death</type>
                     <instance_generator name="UUID"/>
                  </entity>
                  <relationship>crm:P7_took_place_at</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>death/y90s_death_place</source_node>
               <target_node>
                  <entity>
                     <type>crm:E53_Place</type>
                     <instance_generator name="UriExistingOrNew">
                        <arg name="uri" type="xpath">../y90s_death_place_uri/text()</arg>
                        <arg name="text1" type="constant">death_place_uri</arg>
                        <arg name="uri_separator1" type="constant">/</arg>
                     </instance_generator>
                     <label_generator name="Literal">
                        <arg name="text" type="xpath">text()</arg>
                        <arg name="language" type="constant">en</arg>
                     </label_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>y90s_parent_of</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P152i_is_parent_of</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>y90s_parent_of</source_node>
               <target_node>
                  <entity>
                     <type>crm:E21_Person</type>
                     <instance_generator name="UriExistingOrNew">
                        <arg name="uri" type="xpath">text()</arg>
                        <arg name="text1" type="constant">parent_of</arg>
                        <arg name="uri_separator1" type="constant">/</arg>
                     </instance_generator>
                  </entity>
               </target_node>
            </range>
         </link>
         <link>
            <path>
               <source_relation>
                  <relation>y90s_child_of</relation>
               </source_relation>
               <target_relation>
                  <relationship>crm:P152_has_parent</relationship>
               </target_relation>
            </path>
            <range>
               <source_node>y90s_child_of</source_node>
               <target_node>
                  <entity>
                     <type>crm:E21_Person</type>
                     <instance_generator name="UriExistingOrNew">
                        <arg name="uri" type="xpath">text()</arg>
                        <arg name="text1" type="constant">child_of</arg>
                        <arg name="uri_separator1" type="constant">/</arg>
                     </instance_generator>
                  </entity>
               </target_node>
            </range>
         </link>
      </mapping>
   </mappings>
</x3ml>
