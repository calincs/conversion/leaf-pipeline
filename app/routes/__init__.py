from fastapi import APIRouter
from .conversion import router as conversion_router
from .ingestion import router as ingestion_router
from .workflow import router as workflow_router


router = APIRouter()

router.include_router(
    conversion_router,
    prefix='/conversion',
    tags=['conversion'],
)

router.include_router(
    ingestion_router,
    prefix='/ingestion',
    tags=['ingestion'],
)

router.include_router(
    workflow_router,
    prefix='/workflow',
    tags=['workflow'],
)
