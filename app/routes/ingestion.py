from asyncio.log import logger
from app.utils.auth import get_api_key
from app.utils.s3_helper import *
from app.utils.request_helpers import *
from app.utils.rs_request_helpers import *
from app.utils.validate import *
from app.utils.conversions import *
from fastapi import APIRouter, Response, status, Form, File, Security
import json


router = APIRouter()


logger = leaf_pipeline_logging('injestion.py')


@router.post("/")
@router.post("")
# TODO Uncomment this if the input is decided to be a file instead of text
# async def ingestion(response:Response,request_form: bytes = File(...)):
async def ingestion(response: Response, request_form: str = Form(...), api_key: str = Security(get_api_key)):

	# Parse the JSON request and initialize s3_path to the files and graph name
	try:
		request_form_string = json.loads(request_form)
		# TODO Uncomment this if the input is decided to be a file instead of text
		# request_form_string = json.loads(request_form.decode('utf-8'))
	except Exception as err:
		response.status_code = status.HTTP_400_BAD_REQUEST
		return {"message": "Unable to parse JSON", "error": err}

	missing_fields = {}
	missing_field = False
	if 'project' not in request_form_string:
		missing_field = True
		missing_fields["project"] = "missing_field"
	if'public' not in request_form_string:
		missing_field = True
		missing_fields["public"] = "missing_field"
	if "public" in request_form_string:
		if str(request_form_string["public"].lower()) not in ["true", "false"]:
			missing_field = True
			missing_fields["public"] = "invalid_value"
	if missing_field is True:
		response.status_code = status.HTTP_400_BAD_REQUEST
		return {"message": "Missing or invalid field in JSON", "errors": missing_fields}


	project = request_form_string['project']
	publicaton_graph_suffix = ""

	# if the publication status is private then the graph needs to contain public and private data
	# so we go one level out for the path
	# if the publication status is public then the graph only contains public data
	bucket, s3_path_public, _ = get_s3_paths(request_form_string['project'], False, "public")
	if 'public' in request_form_string:
		if str(request_form_string['public']).lower() == "true":
			publication_status = "public"
			s3_path = s3_path_public
		elif str(request_form_string['public']).lower() == "false":
			publicaton_graph_suffix = "_all"
			publication_status = "private"
			s3_path = s3_path_public.replace("public/", "")

	metadata = request_form_string['metadata']

	graph_name = f"{project}{publicaton_graph_suffix}"
	s3_client = get_minio_client()

	# pull list of object names in S3 to the project can be uploaded
	s3_object_pids_generator = s3_client.list_objects(bucket, prefix=s3_path, recursive=True)
	object_pids = []
	s3_object_pids = []
	for s3_pid in s3_object_pids_generator:
		obj_name = str(s3_pid.object_name)
		object_pids.append(obj_name.removeprefix(s3_path).replace("/private", "").replace("/public", ""))
		s3_object_pids.append(obj_name)

	# Check if Graph exists on RS
	logger.info(f"Checking if graph '{graph_name}' exists in triplestore.")
	check_graph = check_graph_exists(f'{graph_name}')
	status_code = 0

	# Error if Graph cannot be checked
	if check_graph == 502:
		response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
		return {"message": f"Failed to check '{graph_name} graph exists in triplestore."}

	# If graph exists, backup graph otherwise move to upload
	if check_graph:
		logger.info(f"Graph '{graph_name}' already exists. Backing up graph.")
		post_url, headers, data, auth = move_graph(f"{graph_name}", f'{graph_name}_temp')
		status_code, request_text = try_rs_request(post_url,
													headers,
													data,
													auth,
													"MOVE Graph to Temp")
		if status_code != 200:
			response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
			return {"message": f"MOVE request to temporary graph '{graph_name}_temp' has failed.", "error": f"{request_text}"}

	# Loop through the list of objects and upload each file into RS
	for pid in s3_object_pids:
		post_url, headers, data, auth = upload_graph_request_data(pid, graph_name)
		status_code, request_text = try_rs_request(post_url,
													headers,
													data,
													auth,
													pid + ".ttl")
		if status_code != 200:
			logger.info(f"Graph '{graph_name}' error adding file with pid {pid}\n{request_text}")
			break

	# If graph exists and the upload fails for at least one file, move temp graph back to original graph
	if check_graph is True and status_code != 200:
		logger.info(f"Upload to RS failed. Moving temp graph '{graph_name}_temp' back to '{graph_name}'.")
		post_url, headers, data, auth = move_graph(f'{graph_name}_temp', graph_name)
		status_code, request_text = try_rs_request(post_url,
													headers,
													data,
													auth,
													"MOVE temp to Named Graph")
		if status_code != 200:
			response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
			logger.info(f"Upload to graph '{graph_name}' has failed. MOVE request to restore graph '{graph_name}_temp' as graph '{graph_name}' failed.\n{request_text}")
			return {"message": f"Upload to graph '{graph_name}' has failed. MOVE request to restore graph '{graph_name}_temp' as graph '{graph_name}' failed.", "error": request_text}
		response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
		return {"message": f"Upload to graph '{graph_name}' has failed.", "error": request_text}

	# If graph didn't exist before and at least one upload fails then we delete the partially created graph and stop
	if check_graph is False and status_code != 200:
		post_url, headers, data, auth = drop_rs_graph(graph_name)
		logger.info(f"Upload request to RS for graph '{graph_name}' failed.")
		response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
		return {"message": f"Upload request to RS for graph '{graph_name}' failed."}

	# Remove temporary graph if upload to RS Succeeds and the graph existed originally
	if check_graph is True and status_code == 200:
		logger.info(f"Dropping temporary graph '{graph_name}_temp'.")
		post_url, headers, data, auth = drop_rs_graph(f"{graph_name}_temp")
		status_code, request_text = try_rs_request(post_url,
													headers,
													data,
													auth,
													"DROP Temp Graph")
		if status_code != 200:
			logger.info(f"DROP GRAPH request for temp graph '{graph_name}_temp' failed.\n{request_text}")

	return {"graph_name": graph_name,
			"ingested_into_RS": True,
			"object_pids": object_pids}
