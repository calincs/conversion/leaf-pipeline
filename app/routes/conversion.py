from asyncio.log import logger
from app.utils.auth import get_api_key
from app.utils.s3_helper import *
from app.utils.request_helpers import *
from app.utils.rs_request_helpers import *
from app.utils.validate import *
from app.utils.conversions import *
from fastapi import APIRouter, File, Response, status, Form, Security
import json


router = APIRouter()

logger = leaf_pipeline_logging('conversion.py')


"""
Replace the following conversion endpoint definition with the following lines if the input parameters are decided to be a file instead of text

async def conversion(response: Response,
					input_file: bytes = File(...),
					request_form: bytes = File(...)):
"""


@router.post("/", status_code=201)
@router.post("", status_code=201)
async def conversion(response: Response, input_file: bytes = File(...), request_form: str = Form(...), api_key: str = Security(get_api_key)):

	# Parse the JSON file and initialize values for the files and S3 client
	try:
		request_form_string = json.loads(request_form)
		# TODO: Un-comment the next line if the input is decided to be a file instead of text
		# request_form_string = json.loads(request_form.decode('utf-8'))
	except Exception as err:
		response.status_code = status.HTTP_400_BAD_REQUEST
		return {"message": "Unable to parse JSON", "error": err}

	# Check that the request contains all required fields
	try:
		missing_fields = {}
		missing_field = False
		if "project" not in request_form_string:
			missing_field = True
			missing_fields["project"] = "missing_field"
		if "workflow" not in request_form_string:
			missing_field = True
			missing_fields["workflow"] = "missing_field"
		if "object_pid" not in request_form_string:
			missing_field = True
			missing_fields["object_pid"] = "missing_field"
		if "public" in request_form_string:
			if str(request_form_string["public"].lower()) not in ["true", "false"]:
				missing_field = True
				missing_fields["public"] = "invalid_value"
		if missing_field is True:
			response.status_code = status.HTTP_400_BAD_REQUEST
			return {"message": "Missing or invalid field in JSON", "errors": missing_fields}
	except Exception as err:
		response.status_code = status.HTTP_400_BAD_REQUEST
		return {"message": "Unable to parse JSON", "error": err}

	file_id = request_form_string['object_pid']
	file_name = request_form_string['object_pid'] + ".ttl"

	# Handle optional public option. Assume False (i.e., draft data only) unless public is true.
	public = "private"
	if 'public' in request_form_string:
		if str(request_form_string['public']).lower() == "true":
			public = "public"

	# Copy the input file into the container's data directory.
	list_of_file_names = [file_name]
	path_to_ttl_output_file = get_path_to_output_ttl_path()  # TODO this is X3ML folder right now regardless of workflow

	# Run the Conversion Module based on the workflow
	# Check for Orlando conversion so we know to save the conversion logs
	if 'encoding' in request_form_string:
		encoding = request_form_string['encoding']
	else:
		encoding = 'utf-8'
	orlando_check = run_conversion(input_file.decode(encoding), request_form_string['workflow'], file_name)

	# Validate the ttl file to see if the converted file is valid
	validate_boolean, text = await validate(list_of_file_names)
	if validate_boolean is False:
		remove_local_file(path_to_ttl_output_file, file_id, 0, file_name)
		logger.error(f"[Job {file_id}] Invalid RDF:\t{text}")
		response.status_code = status.HTTP_400_BAD_REQUEST
		return {"message": f"Unable to convert input file {file_id}. Invalid output RDF.", "error": text}

	# Upload to S3
	s3_client = get_minio_client()
	s3_bucket, s3_file_path, s3_orlando_logs_path = get_s3_paths(request_form_string['project'], orlando_check, public)
	upload_status, upload_error = upload_to_s3(s3_client, s3_bucket, path_to_ttl_output_file, s3_file_path, file_id, file_name, 0)
	if upload_status is False:
		response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
		remove_local_file(path_to_ttl_output_file, file_id, 0, file_name)
		logger.error(f"[Job {file_id}] Unable to upload to S3. Removing input file from container.")
		return {"message": f"Unable to upload converted file to S3 {file_id}. File not updated.", "error": upload_error}

	# Upload orlando logs if an orlando conversion was made
	if orlando_check:
		upload_orlando_logs(s3_bucket, file_id, s3_orlando_logs_path)
	remove_local_file(path_to_ttl_output_file, file_id, 0, file_name)
	return {"object_pid": file_id,
			"converted": True,
			"uploaded_to_s3": True,
			"converted_file_valid": validate_boolean,
			"converted_file_path:": s3_file_path + file_name}


# deleting a certain object deleted the whole file. -- maybe because it deleted all files that contained the name instead of exact match. only an issue when only the file is given without the project.

@router.delete("/", status_code=204)
@router.delete("", status_code=204)
# TODO Uncomment this if the input is decided to be a file instead of text
# async def delete(response: Response, request_form: bytes = File(...)):
async def delete(response: Response, request_form: str = Form(...), api_key: str = Security(get_api_key)):
	try:
		request_form_string = json.loads(request_form)
		# TODO Uncomment this if the input is decided to be a file instead of text
		# request_form_string = json.loads(request_form.decode('utf-8'))
	except Exception as err:
		response.status_code = status.HTTP_400_BAD_REQUEST
		return {"message": "Unable to parse JSON", "error": err}

	list_deleted_files = []
	s3_client = get_minio_client()

	missing_fields = {}
	if 'project' not in request_form_string:
		# if project is not provided then object_pid is required
		missing_fields["project"] = "missing_field"
		if 'object_pid' not in request_form_string:
			response.status_code = status.status.HTTP_400_BAD_REQUEST
			missing_fields["object_pid"] = "missing_field"

			return {"message": "Missing field in JSON", "errors": missing_fields}

		else:
			# if project is not provided, but object_pid is provided
			# then we need to look for the object everywhere
			bucket, s3_path, _ = get_s3_paths(None, False, "private")

			# gets the lists of object from S3
			removed = 1  # if any file is not deleted then this will be 0 and the whole job will return 404
			get_objects = s3_client.list_objects(bucket, prefix=s3_path, recursive=True)
			for obj in get_objects:
				if obj.object_name.endswith(request_form_string['object_pid'] + ".ttl"):
					check_remove = remove_from_s3_cleanup(s3_client, bucket, obj.object_name, "None")
					if check_remove == 0:
						removed = 0
					else:
						logging.info(f"Removed {obj.object_name} from S3 folder {s3_path}")
			if removed == 0:
				response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
				return Response(status_code=status.HTTP_204_NO_CONTENT)
			return Response(status_code=status.HTTP_204_NO_CONTENT)


	# project provided
	else:
		bucket, s3_path_public, _ = get_s3_paths(request_form_string['project'], False, "public")
		_, s3_path_private, _ = get_s3_paths(request_form_string['project'], False, "private")

		# Check if object_pid exists and delete that object from S3 if it exists
		if 'object_pid' in request_form_string:
			delete_status = 0
			# need to check public and private bucket
			# a file should only be in one, but if something went wrong it could be in both
			for s3_path in [s3_path_public, s3_path_private]:
				get_objects = s3_client.list_objects(bucket, prefix=s3_path, recursive=True)
				for obj in get_objects:
					# if object matches
					if obj.object_name.find(request_form_string['object_pid']) > -1:
						deleted = remove_from_s3(s3_client, bucket, s3_path, f"{request_form_string['object_pid']}.ttl", f"{request_form_string['object_pid']}", 0)
						if deleted == 1:
							delete_status += 1
							logging.info(f"[{request_form_string['object_pid']}] File deleted from S3 folder {s3_path}")
							break
						else:
							response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
							return {"message": f"{request_form_string['object_pid']}] Unable to delete file from S3 folder {s3_path}"}

			if delete_status > 0:
				return Response(status_code=status.HTTP_204_NO_CONTENT)
			else:
				# file not found. Still return 204
				return Response(status_code=status.HTTP_204_NO_CONTENT)

		# If object_pid is not provided, delete all objects from S3 from that project
		else:
			# gets the lists of object from S3
			removed = 1  # if any file is not deleted then this will be 0 and the whole job will return 404
			for s3_path in [s3_path_public, s3_path_private]:
				objects_to_delete = s3_client.list_objects(bucket, prefix=s3_path, recursive=True)
				for obj in objects_to_delete:
					list_deleted_files.append(obj.object_name)
					check_remove = remove_from_s3_cleanup(s3_client, bucket, obj.object_name, "None")
					if check_remove == 0:
						removed = 0
					else:
						logging.info(f"Removed {obj.object_name} from S3 folder {s3_path}")
			# if any file wasn't deleted, return 500
			if removed == 0:
				response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
				return {"message": f"{request_form_string['object_pid']}] Unable to delete file from S3 folder {s3_path}"}
			return Response(status_code=status.HTTP_204_NO_CONTENT)
