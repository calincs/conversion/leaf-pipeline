from asyncio.log import logger
from app.utils.rs_request_helpers import leaf_pipeline_logging
from fastapi import APIRouter
import json


router = APIRouter()

logger = leaf_pipeline_logging('workflow.py')


# gets the list workflows through the workflow.json
@router.get("/", status_code=200)
@router.get("", status_code=200)
def workflow():

	# returned the parsed json file
	return json.load(open('app/data/workflow_configuration/workflows.json'))
